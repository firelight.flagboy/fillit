/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_all_tetris.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/17 12:35:52 by fbenneto          #+#    #+#             */
/*   Updated: 2017/11/21 14:04:41 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			ft_pow(int n, int p)
{
	if (p == 0)
		return (1);
	return (n * ft_pow(n, p - 1));
}

int			ft_char_to_dec(char *str)
{
	int		i;
	int		j;
	int		dec;
	char	bin[17];

	i = 0;
	j = -1;
	dec = 0;
	while (str[i])
	{
		if (str[i] == '.')
			bin[++j] = '0';
		else if (str[i] == '#')
			bin[++j] = '1';
		i++;
	}
	bin[++j] = '\0';
	j--;
	while (j >= 0)
	{
		if (bin[j] == '1')
			dec += ft_pow(2, j);
		j--;
	}
	return (dec);
}

static void	check_form(int nb)
{
	if (!(ft_is_carre(nb) || ft_is_barre_v(nb) || ft_is_barre_h(nb)
				|| ft_is_t_top(nb) || ft_is_t_bottom(nb) || ft_is_t_right(nb)
				|| ft_is_t_left(nb) || ft_is_z_right(nb) || ft_is_z_left(nb)
				|| ft_is_z_top_right(nb) || ft_is_z_top_left(nb)
				|| ft_is_l_v_bottom_right(nb) || ft_is_l_v_bottom_left(nb)
				|| ft_is_l_v_top_right(nb) || ft_is_l_v_top_left(nb)
				|| ft_is_l_h_bottom_right(nb) || ft_is_l_h_bottom_left(nb)
				|| ft_is_l_h_top_right(nb) || ft_is_l_h_top_left(nb)))
		ft_error();
}

int			ft_check_one_tetris(char *str)
{
	int		nb;
	int		i;

	nb = ft_char_to_dec(str);
	while (*str)
	{
		i = 0;
		while (*str == '.' || *str == '#')
		{
			i++;
			str++;
		}
		if (i != 4)
			ft_error();
		if (*str == '\0')
			break ;
		if (*str != '.' && *str != '\n' && *str != '#')
			ft_error();
		str++;
	}
	check_form(nb);
	return (1);
}

int			ft_check_all_tetris(char *str)
{
	char	ret[21];
	int		i;
	int		j;

	i = 0;
	if (str[i] == '\0')
		ft_error();
	while (str[i])
	{
		j = 0;
		if (str[i] == '\n')
			i++;
		if (str[i] == '\n')
			ft_error();
		while (str[i] && !(str[i] == '\n' && (str[i + 1] == '\n'
						|| str[i + 1] == '\0')))
			ret[j++] = str[i++];
		ret[j] = '\0';
		if (j > 19)
			ft_error();
		ft_check_one_tetris(ret);
		i++;
	}
	return (1);
}
