/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/17 15:16:08 by fbenneto          #+#    #+#             */
/*   Updated: 2017/11/21 15:08:43 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_get_min_map_size(t_tetris *lst)
{
	int nb_elem;
	int nb_blocs;
	int i;

	i = 2;
	nb_elem = ft_get_len_lst(lst);
	nb_blocs = nb_elem * 4;
	while (i < 12)
	{
		if (nb_blocs <= i * i)
			return (i);
		i++;
	}
	return (2);
}

void	ft_print_map(char **map)
{
	int line;

	line = -1;
	while (map[++line])
		ft_putendl(map[line]);
	ft_free_map(map);
	exit(EXIT_SUCCESS);
}

char	**ft_getmap(int size)
{
	int		i;
	int		c;
	char	**map;

	if (!(map = (char**)malloc((size + 1) * sizeof(char*))))
		return (NULL);
	map[size] = 0;
	i = -1;
	while (++i < size)
	{
		if (!(map[i] = ft_strnew(size)))
			return (NULL);
		c = -1;
		while (++c < size)
			map[i][c] = '.';
	}
	return (map);
}

char	**ft_place_on_map(char **map, char *tetris, t_pos pos, int size)
{
	char	**res;
	int		row;
	int		line;

	if (!(res = ft_mapcpy(map, size)))
		exit(EXIT_FAILURE);
	size = -1;
	line = pos.line;
	row = pos.row;
	while (tetris[++size])
	{
		if (tetris[size] == '\n')
		{
			row = pos.row;
			line++;
		}
		else if (tetris[size] != '.')
			res[line][row] = tetris[size];
		if (tetris[size] != '\n')
			row++;
	}
	return (res);
}
