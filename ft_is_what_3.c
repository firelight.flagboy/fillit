/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_what_3.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/18 12:55:11 by fbenneto          #+#    #+#             */
/*   Updated: 2017/11/21 11:53:55 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_is_z_top_left(int n)
{
	if (n == 561 || n == 1122 || n == 2244 || n == 8976 || n == 17952\
		|| n == 35904)
		return (1);
	return (0);
}

int		ft_is_l_v_bottom_right(int n)
{
	if (n == 785 || n == 1570 || n == 3140 || n == 12560 || n == 50240\
		|| n == 25120)
		return (1);
	return (0);
}

int		ft_is_l_v_bottom_left(int n)
{
	if (n == 802 || n == 1604 || n == 3208 || n == 12832 || n == 51328\
		|| n == 25664)
		return (1);
	return (0);
}

int		ft_is_l_v_top_right(int n)
{
	if (n == 275 || n == 550 || n == 1100 || n == 4400 || n == 17600\
		|| n == 8800)
		return (1);
	return (0);
}

int		ft_is_l_v_top_left(int n)
{
	if (n == 547 || n == 1094 || n == 2188 || n == 8752 || n == 17504\
		|| n == 35008)
		return (1);
	return (0);
}
