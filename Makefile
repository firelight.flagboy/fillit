# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vbaudot <vbaudot@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/17 08:40:58 by vbaudot           #+#    #+#              #
#    Updated: 2017/11/21 15:11:51 by fbenneto         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
CFLAGS=-Wall -Werror -Wextra
NAME = fillit
LIB = libft/libft.a
DLIB = libft
DSRC = Dfillit
SRCS = main.c ft_print_use.c ft_fillit.c\
	ft_error.c ft_check_all_tetris.c ft_get_tetris.c\
	ft_free.c ft_backtrack.c ft_map.c\
	ft_is_what_1.c ft_is_what_2.c ft_is_what_3.c\
	ft_is_what_4.c ft_init_tab.c ft_duptetris.c\
	ft_mapcpy.c
OBJS = $(SRCS:.c=.o)

YEL="\\033[33m"
BLU="\\033[34m"
RED="\\033[31m"
NC="\\033[0m"
MAG="\\033[35m"
CYA="\\033[36m"
GRE="\\033[32m"
BOL="\\033[1m"
CHE="\\xE2\\x9C\\x94"
OK="$(GRE)$(CHE)$(NC)"

all: $(NAME)

$(NAME): $(LIB) $(OBJS)
	@printf "\nlinking $(CYA)$(BOL)$(LIB) $(OBJS)$(NC)\n"
	@printf "to make the binary $(MAG)$(BOL)$(NAME)$(NC)\n"
	@$(CC) $(CFLAGS) $^ -o $@

$(LIB):
	@make -C $(DLIB) libft.a

%.o : %.c
	@printf "\r\033[0K[fillit] compile $(BOL)$(YEL)$<$(NC)..."
	@$(CC) $(CFLAGS) -I./libft -c $<
	@printf '\t\t'$(OK)

clean:
	@make -C $(DLIB) $@
	@printf "[fillit] rm all $(BOL)$(RED) obj file$(NC)"
	@rm -f $(OBJS)
	@printf '\t\t'$(OK)'\n'

fclean: clean
	@make -C $(DLIB) cleanf
	@printf "[fillit] rm $(BOL)$(CYA)$(NAME)$(NC)"
	@rm -f $(NAME)
	@printf '\t\t\t'$(OK)'\n'

re: fclean all

proper : all clean

.PHONY: re proper
