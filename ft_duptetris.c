/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_duptetris.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/20 11:46:12 by fbenneto          #+#    #+#             */
/*   Updated: 2017/11/21 12:21:15 by vbaudot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int	ft_is_good_char(char c)
{
	if (c == '\n' || c == '.' || c == '#' || c == '\0')
		return (1);
	return (0);
}

char		*ft_duptetris(char **str, size_t len)
{
	char	*res;
	char	*tmp;
	int		i;

	if (!(res = ft_strnew(20)))
		exit(EXIT_FAILURE);
	i = 0;
	tmp = *str;
	while (i < 20 && i < (int)len)
	{
		if (ft_is_good_char(tmp[i]))
			res[i] = tmp[i];
		else
			ft_error();
		i++;
	}
	if (len <= 20)
		**str = 0;
	else
		*str = tmp + i + 1;
	return (res);
}
