/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/18 15:54:40 by fbenneto          #+#    #+#             */
/*   Updated: 2017/11/21 12:17:27 by vbaudot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_fc	*ft_init_tab(void)
{
	t_fc	*tab;

	if (!(tab = (t_fc*)malloc(20 * sizeof(t_fc))))
		exit(EXIT_FAILURE);
	ft_init_tab_fc(&tab);
	ft_init_tab_str(&tab);
	return (tab);
}

void	ft_init_tab_fc(t_fc **tab)
{
	t_fc *tmp;

	tmp = *tab;
	tmp[0].ft_is_what = ft_is_carre;
	tmp[1].ft_is_what = ft_is_barre_v;
	tmp[2].ft_is_what = ft_is_barre_h;
	tmp[3].ft_is_what = ft_is_t_top;
	tmp[4].ft_is_what = ft_is_t_bottom;
	tmp[5].ft_is_what = ft_is_t_right;
	tmp[6].ft_is_what = ft_is_t_left;
	tmp[7].ft_is_what = ft_is_z_right;
	tmp[8].ft_is_what = ft_is_z_left;
	tmp[9].ft_is_what = ft_is_z_top_right;
	tmp[10].ft_is_what = ft_is_z_top_left;
	tmp[11].ft_is_what = ft_is_l_v_bottom_right;
	tmp[12].ft_is_what = ft_is_l_v_bottom_left;
	tmp[13].ft_is_what = ft_is_l_v_top_right;
	tmp[14].ft_is_what = ft_is_l_v_top_left;
	tmp[15].ft_is_what = ft_is_l_h_bottom_right;
	tmp[16].ft_is_what = ft_is_l_h_bottom_left;
	tmp[17].ft_is_what = ft_is_l_h_top_right;
	tmp[18].ft_is_what = ft_is_l_h_top_left;
	tmp[19].ft_is_what = NULL;
}

void	ft_init_tab_str(t_fc **tab)
{
	t_fc *tmp;

	tmp = *tab;
	tmp[0].type_str = STR_CARRE;
	tmp[1].type_str = STR_BAR_V;
	tmp[2].type_str = STR_BAR_H;
	tmp[3].type_str = STR_T_TOP;
	tmp[4].type_str = STR_T_BOTTOM;
	tmp[5].type_str = STR_T_RIGHT;
	tmp[6].type_str = STR_T_LEFT;
	tmp[7].type_str = STR_Z_RIGHT;
	tmp[8].type_str = STR_Z_LEFT;
	tmp[9].type_str = STR_Z_TOP_RIGHT;
	tmp[10].type_str = STR_Z_TOP_LEFT;
	tmp[11].type_str = STR_L_V_BOTTOM_RIGHT;
	tmp[12].type_str = STR_L_V_BOTTOM_LEFT;
	tmp[13].type_str = STR_L_V_TOP_RIGHT;
	tmp[14].type_str = STR_L_V_TOP_LEFT;
	tmp[15].type_str = STR_L_H_BOTTOM_RIGHT;
	tmp[16].type_str = STR_L_H_BOTTOM_LEFT;
	tmp[17].type_str = STR_L_H_TOP_RIGHT;
	tmp[18].type_str = STR_L_H_TOP_LEFT;
	tmp[19].type_str = 0;
}
