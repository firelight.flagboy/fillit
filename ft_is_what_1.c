/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_what_1.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/18 12:55:11 by fbenneto          #+#    #+#             */
/*   Updated: 2017/11/18 14:02:39 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_is_carre(int n)
{
	if (n == 51 || n == 102 || n == 204 || n == 816 || n == 1632 || n == 3264\
		|| n == 13056 || n == 26112 || n == 52224)
		return (1);
	return (0);
}

int		ft_is_barre_v(int n)
{
	if (n == 4369 || n == 8738 || n == 17476 || n == 34952)
		return (1);
	return (0);
}

int		ft_is_barre_h(int n)
{
	if (n == 15 || n == 240 || n == 3840 || n == 61440)
		return (1);
	return (0);
}

int		ft_is_t_top(int n)
{
	if (n == 114 || n == 228 || n == 1824 || n == 3648 || n == 58368\
		|| n == 29184)
		return (1);
	return (0);
}

int		ft_is_t_bottom(int n)
{
	if (n == 39 || n == 78 || n == 624 || n == 1248 || n == 19968 || n == 9984)
		return (1);
	return (0);
}
