/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mapcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/21 11:55:54 by fbenneto          #+#    #+#             */
/*   Updated: 2017/11/21 11:57:25 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char		**ft_mapcpy(char **map, int size)
{
	char	**res;
	int		row;
	int		line;

	if (!(res = ft_getmap(size)))
		exit(EXIT_FAILURE);
	line = -1;
	while (++line < size)
	{
		row = -1;
		while (++row < size)
			res[line][row] = map[line][row];
	}
	return (res);
}
