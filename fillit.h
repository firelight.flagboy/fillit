/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbaudot <vbaudot@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/17 08:54:45 by vbaudot           #+#    #+#             */
/*   Updated: 2017/11/21 12:19:47 by vbaudot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# define STR_CARRE "##\n##"
# define STR_BAR_V "#\n#\n#\n#"
# define STR_BAR_H "####"
# define STR_T_TOP ".#.\n###"
# define STR_T_BOTTOM "###\n.#."
# define STR_T_RIGHT "#.\n##\n#."
# define STR_T_LEFT ".#\n##\n.#"
# define STR_Z_RIGHT ".##\n##."
# define STR_Z_LEFT "##.\n.##"
# define STR_Z_TOP_RIGHT ".#\n##\n#."
# define STR_Z_TOP_LEFT "#.\n##\n.#"
# define STR_L_V_BOTTOM_RIGHT "#.\n#.\n##"
# define STR_L_V_BOTTOM_LEFT ".#\n.#\n##"
# define STR_L_V_TOP_RIGHT "##\n#.\n#."
# define STR_L_V_TOP_LEFT "##\n.#\n.#"
# define STR_L_H_BOTTOM_RIGHT "###\n..#"
# define STR_L_H_BOTTOM_LEFT "###\n#.."
# define STR_L_H_TOP_RIGHT "..#\n###"
# define STR_L_H_TOP_LEFT "#..\n###"

# include "libft.h"
# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include <sys/types.h>
# include <sys/stat.h>

typedef	struct		s_pos
{
	int				row;
	int				line;
}					t_pos;

typedef	struct		s_tetris
{
	char			*tetri;
	char			letter;
	struct s_tetris	*next;
	int				index;
}					t_tetris;

typedef	struct		s_fc
{
	char			*type_str;
	int				(*ft_is_what)(int);
}					t_fc;

void				ft_print_use(void);
void				ft_fillit(char const *filename);
int					ft_char_to_dec(char *str);
int					ft_check_all_tetris(char *str);
void				ft_error(void);
t_tetris			*ft_get_lst_tetris(char *str);
void				ft_dellst(t_tetris **lst);
void				ft_free_map(char **map);
void				ft_print_map(char **map);
char				**ft_getmap(int size);
void				ft_backtrack(t_tetris *lst);
char				**ft_place_on_map(char **map, char *tetris,\
									t_pos pos, int size);
int					ft_get_len_lst(t_tetris *lst);
char				*ft_duptetris(char **str, size_t len);
int					ft_get_min_map_size(t_tetris *lst);
int					ft_is_carre(int n);
int					ft_is_barre_v(int n);
int					ft_is_barre_h(int n);
int					ft_is_t_top(int n);
int					ft_is_t_bottom(int n);
int					ft_is_t_right(int n);
int					ft_is_t_left(int n);
int					ft_is_z_right(int n);
int					ft_is_z_left(int n);
int					ft_is_z_top_right(int n);
int					ft_is_z_top_left(int n);
int					ft_is_l_v_bottom_right(int n);
int					ft_is_l_v_bottom_left(int n);
int					ft_is_l_v_top_right(int n);
int					ft_is_l_v_top_left(int n);
int					ft_is_l_h_bottom_right(int n);
int					ft_is_l_h_bottom_left(int n);
int					ft_is_l_h_top_right(int n);
int					ft_is_l_h_top_left(int n);
t_fc				*ft_init_tab(void);
void				ft_init_tab_str(t_fc **tab);
void				ft_init_tab_fc(t_fc **tab);
char				**ft_mapcpy(char **map, int size);
#endif
