/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fillit.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/17 10:51:46 by fbenneto          #+#    #+#             */
/*   Updated: 2017/11/20 13:03:35 by vbaudot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		ft_fillit(char const *filename)
{
	int			fd;
	char		*res;
	char		*str;
	t_tetris	*lst;

	if ((fd = open(filename, O_RDONLY)) == -1)
		ft_error();
	filename = 0;
	if (!(res = ft_read_file(fd)))
		ft_error();
	close(fd);
	if (!(str = ft_strdup(res)))
		ft_error();
	free(res);
	if (!(ft_check_all_tetris(str)))
	{
		free(str);
		ft_error();
	}
	lst = ft_get_lst_tetris(str);
	ft_backtrack(lst);
	ft_dellst(&lst);
	free(str);
}
