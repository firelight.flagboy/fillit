/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_tetris.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/17 13:46:56 by fbenneto          #+#    #+#             */
/*   Updated: 2017/11/21 15:06:25 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int				ft_get_len_lst(t_tetris *lst)
{
	return ((lst) ? 1 + ft_get_len_lst(lst->next) : 0);
}

static t_tetris	*ft_getnew_elem(void)
{
	t_tetris	*res;

	if (!(res = (t_tetris*)malloc(sizeof(t_tetris))))
		return (NULL);
	res->tetri = NULL;
	res->next = 0;
	res->letter = '\0';
	return (res);
}

static char		*ft_change_char(char *str, char letter)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] == '#')
			str[i] = letter;
		i++;
	}
	return (str);
}

int				ft_call_fc(int nb, t_tetris *current, char letter)
{
	t_fc	*tab;
	int		i;

	tab = ft_init_tab();
	i = 0;
	while (tab[i].type_str)
	{
		if (tab[i].ft_is_what(nb))
		{
			current->tetri = ft_change_char(ft_strdup(tab[i].type_str), letter);
			current->letter = letter;
			break ;
		}
		i++;
	}
	free(tab);
	return (i);
}

t_tetris		*ft_get_lst_tetris(char *str)
{
	t_tetris	*res;
	t_tetris	*current;
	int			nb;
	char		letter;

	if (!(res = ft_getnew_elem()))
		return (NULL);
	current = res;
	letter = 'A';
	while (*str)
	{
		nb = ft_char_to_dec(ft_duptetris(&str, ft_strlen(str)));
		if (ft_call_fc(nb, current, letter++) <= 20 && *str)
		{
			if (ft_get_len_lst(res) <= 26)
			{
				if (!(current->next = ft_getnew_elem()))
					return (NULL);
			}
			else
				ft_error();
		}
		current = current->next;
	}
	return (res);
}
