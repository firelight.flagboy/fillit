/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_what_4.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/18 12:55:11 by fbenneto          #+#    #+#             */
/*   Updated: 2017/11/21 11:53:37 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_is_l_h_bottom_right(int n)
{
	if (n == 71 || n == 142 || n == 1136 || n == 36352 || n == 18176\
		|| n == 2272)
		return (1);
	return (0);
}

int		ft_is_l_h_bottom_left(int n)
{
	if (n == 23 || n == 46 || n == 368 || n == 11776 || n == 5888 || n == 736)
		return (1);
	return (0);
}

int		ft_is_l_h_top_right(int n)
{
	if (n == 116 || n == 232 || n == 1856 || n == 3712 || n == 29696\
		|| n == 59392)
		return (1);
	return (0);
}

int		ft_is_l_h_top_left(int n)
{
	if (n == 113 || n == 226 || n == 1808 || n == 3616 || n == 28928\
		|| n == 57856)
		return (1);
	return (0);
}
