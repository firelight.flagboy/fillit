/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/17 15:01:49 by fbenneto          #+#    #+#             */
/*   Updated: 2017/11/17 16:33:59 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_dellst(t_tetris **lst)
{
	t_tetris	*node;
	t_tetris	*tmp;

	if (!lst)
		return ;
	node = *lst;
	while (node)
	{
		free(node->tetri);
		tmp = node;
		node = node->next;
		free(tmp);
	}
	*lst = 0;
}

void	ft_free_map(char **map)
{
	int i;

	i = -1;
	while (map[++i])
		free(map[i]);
	free(map);
}
