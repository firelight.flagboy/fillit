/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_backtrack.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/17 15:08:58 by fbenneto          #+#    #+#             */
/*   Updated: 2017/11/21 15:09:22 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_can_place(char **map, char *tetris, t_pos pos, int size)
{
	int i;
	int row;

	i = -1;
	row = pos.row;
	while (tetris[++i])
	{
		if (row > size || pos.line >= size)
			return (0);
		if (tetris[i] == '.')
			row++;
		else if (tetris[i] == '\n')
		{
			row = pos.row;
			pos.line++;
		}
		else if (map[pos.line][row] != '.')
			return (0);
		else
			row++;
	}
	return (1);
}

int		ft_place_tetris(char **map, t_tetris *lst, int size_m)
{
	t_pos	pos;
	char	**tmp;

	if (!lst)
		ft_print_map(map);
	else
	{
		pos.line = -1;
		while (++pos.line < size_m)
		{
			pos.row = -1;
			while (++pos.row < size_m)
			{
				if (ft_can_place(map, lst->tetri, pos, size_m))
				{
					tmp = ft_place_on_map(map, lst->tetri, pos, size_m);
					ft_place_tetris(tmp, lst->next, size_m);
				}
			}
		}
	}
	ft_free_map(map);
	return ((pos.line >= size_m) ? -1 : 0);
}

void	ft_backtrack(t_tetris *lst)
{
	char	**map;
	int		i;

	i = ft_get_min_map_size(lst);
	if (!(map = ft_getmap(i)))
		exit(EXIT_FAILURE);
	while (ft_place_tetris(map, lst, i++) == -1)
		if (!(map = ft_getmap(i)))
			exit(EXIT_FAILURE);
}
